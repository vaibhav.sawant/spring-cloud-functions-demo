package com.tw.demo.builder;

import org.springframework.messaging.Message;

public class MessageBuilder {

    public static Message<Double> createMessage(Double payload) {
        return org.springframework.messaging.support.MessageBuilder
            .withPayload(payload)
            .build();
    }
}
