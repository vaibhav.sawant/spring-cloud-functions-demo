package com.tw.demo.listner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;

import java.util.function.Consumer;

@Slf4j
@Configuration
public class EventConsumer {

    @Bean
    public Consumer<Message<Double>> transactionConsumer() {
        return (message) -> log.info("Transaction received with payload: {}", message.getPayload());
    }
}
