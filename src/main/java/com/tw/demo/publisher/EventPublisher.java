package com.tw.demo.publisher;

import com.tw.demo.builder.MessageBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;

import java.util.Random;
import java.util.function.Supplier;

@Slf4j
@Configuration
public class EventPublisher {

    @Bean
    public Supplier<Message<Double>> transactionPublisher() {
        return () -> {
            double payload = new Random().nextDouble();
            log.info("Published transaction with amount : {}", payload);
            return MessageBuilder.createMessage(payload);
        };
    }

//    @Bean
//    public Supplier<Flux<Message<Double>>> transactionReactivePublisher() {
//        return () -> {
//            double payload = new Random().nextDouble();
//            log.info("Published transaction using Flux supplier with amount : {}", payload);
//            return Flux.just(MessageBuilder.createMessage(payload));
//        };
//    }
}
