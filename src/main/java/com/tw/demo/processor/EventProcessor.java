package com.tw.demo.processor;

import com.tw.demo.builder.MessageBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;

import java.util.function.Function;

@Slf4j
@Configuration
public class EventProcessor {

    @Bean
    public Function<Message<Double>, Message<Double>> transactionProcessor() {
        return (transactionAmountMessage) -> {
            var transactionAmount = transactionAmountMessage.getPayload();
            log.info("Transforming transaction with amount : {}", transactionAmount);

            return MessageBuilder.createMessage(transactionAmount * 10);
        };
    }

}
